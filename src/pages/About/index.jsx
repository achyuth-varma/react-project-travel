import React, { useContext, useState } from "react";
import { ThemeContext } from "../../ThemeContext";
import WhiteMountain from "../../images/NewWhiteMountains.jpg";
import "./about.css";

const About = () => {
  const theme = useContext(ThemeContext);

  const [validEmail, setEmailValid] = useState(false);

  const [validName, setNameValid] = useState(false);

  const [gender, setGender] = useState("");

  const emailOnChange = (event) => {
    const { value } = event.target;

    const regEmail = new RegExp(
      //eslint-disable-next-line
      /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
    );

    if (regEmail.test(value)) {
      setEmailValid(true);
    } else {
      setEmailValid(false);
    }
  };

  const nameOnChange = (event) => {
    const { value } = event.target;

    if (value.length > 0) {
      setNameValid(true);
    } else {
      setNameValid(false);
    }
  };

  const onSelectChange = (event) => {
    setGender(event.target.value);
  };

  return (
    <div className={`home home-${theme}`}>
      <section className="home-hero-image-box">
        <div className="hero-image-box-home">
          <span>About Me</span>
        </div>
        <img
          src={WhiteMountain}
          alt="Another Set of mountains"
          className="home-hero-image about-altercation"
        />
      </section>
      <section className="about-me-info">
        <h2>Fill out this form to get the details about me</h2>
        <h3>Change the gender dropdown to get another conditional dropbox</h3>
        <form className="form-box">
          <div className="form-field-flexbox">
            <label htmlFor="full-name">Full Name</label>
            <div className="email-input-box">
              <input
                id="full-name"
                placeholder="Full Name"
                className={`input-field input-${theme}`}
                onChange={nameOnChange}
              />
              {validName === false ? (
                <span className="error-message">*Please give valid name</span>
              ) : (
                ""
              )}
            </div>
          </div>
          <div className="form-field-flexbox">
            <label htmlFor="email">Email</label>
            <div className="email-input-box">
              <input
                id="email"
                placeholder="Email"
                className={`input-field input-${theme}`}
                onChange={emailOnChange}
              />
              {validEmail === false ? (
                <span className="error-message">*Please give valid email</span>
              ) : (
                ""
              )}
            </div>
          </div>

          <div className="form-field-flexbox">
            <label htmlFor="gender-dropdown">Gender</label>
            <select
              id="gender-dropdown"
              className={`gender-dropdown input-${theme}`}
              onChange={onSelectChange}
            >
              <option defaultChecked value="male">
                Male
              </option>
              <option value="female">Female</option>
              <option value="other">Other</option>
            </select>
          </div>
          {/* When Male gender is selected */}
          {gender === "male" ? (
            <div className="form-field-flexbox change-dropdown-animation">
              <label htmlFor="male-prefix">Prefix - Male</label>
              <select
                id="male-prefix"
                className={`gender-dropdown input-${theme} `}
              >
                <option defaultChecked value="mister">
                  Mr.
                </option>
                <option value="doctor">Dr. </option>
                <option value="other">Other</option>
              </select>
            </div>
          ) : (
            ""
          )}

          {/* When Female is selected */}
          {gender === "female" ? (
            <div className="form-field-flexbox change-dropdown-animation">
              <label htmlFor="female-prefix">Prefix - Female</label>
              <select
                id="female-prefix"
                className={`gender-dropdown input-${theme}`}
              >
                <option defaultChecked value="miss">
                  Ms.
                </option>
                <option value="misses">Mrs. </option>
                <option value="doctor">Doctor</option>
              </select>
            </div>
          ) : (
            ""
          )}

          {gender === "other" ? (
            <div className="form-field-flexbox change-dropdown-animation">
              <label htmlFor="other-field">Prefix - Other</label>
              <input
                id="other-field"
                placeholder="Please Specify"
                className={`input-field input-${theme}`}
              />
            </div>
          ) : (
            ""
          )}
          <div className="submit-button-box">
            <button
              onClick={(event) => {
                event.preventDefault();
              }}
            >
              Submit
            </button>
          </div>
        </form>
      </section>
    </div>
  );
};

export default About;
