import React, { useContext } from "react";
import { ThemeContext } from "../../ThemeContext";
import "./home.css";
import Mountain from "../../images/mountains.jpg";
import RedChairs from "../../images/red-chair-lady.jpg";
import PhotographerMountain from "../../images/photographer-mountain.jpg";
import Kids from "../../images/kids-snow.jpg";
import LadySnow from "../../images/LadySnow.jpg";
import ManInLondon from "../../images/ManInLondon.jpg";
import WomanInParis from "../../images/WomanInParis.jpg";
import { ReactComponent as Travellers } from "../../images/svgs/Travellers.svg";
import { ReactComponent as Writing } from "../../images/svgs/Writing.svg";

const Home = ({ callback }) => {
  const theme = useContext(ThemeContext);

  return (
    <div className={`home home-${theme}`}>
      <section className="home-hero-image-box">
        <div className="hero-image-box-home">
          <span>Home</span>
        </div>
        <img src={Mountain} alt="mountains" className="home-hero-image" />
      </section>
      <section className="home-content-section">
        <h1 className="home-content-section-main-heading">
          Welcome to the world of travel blogs
        </h1>

        <div className="home-image-text">
          <div className="home-judgement-flexbox">
            <h2>
              Welcome to the judgement free community, meet someone who shares
              the same boat
            </h2>
            <div className="home-judgement-illustration-size">
              <Travellers />
            </div>
          </div>

          <div
            className="home-judgement-flexbox"
            style={{ flexDirection: "row-reverse" }}
          >
            <h2>Share your experience with your friends and partners</h2>
            <div className="home-judgement-illustration-size">
              <Writing />
            </div>
          </div>
        </div>

        <div className="gallery-of-images-box">
          <h3>Here is the quick peek of images</h3>

          <div className="home-gallery-of-images">
            <div>
              <img src={RedChairs} alt={RedChairs} />
            </div>
            <div>
              <img src={Kids} alt={Kids} />
            </div>
            <div>
              <img src={PhotographerMountain} alt={PhotographerMountain} />
            </div>
            <div>
              <img src={LadySnow} alt={LadySnow} />
            </div>
            <div>
              <img src={ManInLondon} alt={ManInLondon} />
            </div>
            <div>
              <img src={WomanInParis} alt={WomanInParis} />
            </div>
          </div>

          <button
            className={`button-2 button-2-${theme}`}
            onClick={() => {
              callback("blogs");
            }}
          >
            Check Out Blogs
          </button>
        </div>
      </section>
    </div>
  );
};

export default Home;
