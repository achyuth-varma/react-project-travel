import React, { useContext } from "react";
import { ThemeContext } from "../../ThemeContext";
import "./blogs.css";
import BlogsMountain from "../../images/BlogsMountain.jpg";
import data from "../../data/blogs.json";
import MiniBlog from "../../components/miniblog";

const Blogs = () => {
  const theme = useContext(ThemeContext);

  return (
    <div className={`home home-${theme}`}>
      <section className="home-hero-image-box">
        <div className="hero-image-box-home">
          <span>Mini-Blogs</span>
        </div>
        <img
          src={BlogsMountain}
          alt="Another Set of mountains"
          className="home-hero-image"
        />
      </section>
      <section className="blogs-content-section">
        <h1 className="blogs-content-section-heading">Mini Blogs</h1>

        <div className="cards-mini-card-layout">
          <h3>
            You can view all the blogs that the people in this community has
            submitted
          </h3>

          <div className="mini-blogs-container">
            {data.map((value) => (
              <MiniBlog value={value} />
            ))}
          </div>
        </div>
      </section>
    </div>
  );
};

export default Blogs;
