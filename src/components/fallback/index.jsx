import React from "react";

const FallBack = () => {
  return (
    <div>
      <h2>Loading...</h2>
    </div>
  );
};

export default FallBack;
