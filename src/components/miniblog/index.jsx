import React from "react";
import "./miniblog.css";

const MiniBlog = ({ value }) => {
  return (
    <div className="card-layout">
      <img src={value.imagePath} alt={value.place} />
      <div className="card-content">
        <h2> {value.author} </h2>
        <p>{value.experience}</p>
        <button
          className="locate-me-button"
          onClick={(event) => {
            event.preventDefault();
            window.location = value.location;
          }}
        >
          Locate Me
        </button>
      </div>
    </div>
  );
};

export default MiniBlog;
