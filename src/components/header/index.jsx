import React, { useContext, useState } from "react";
import { ThemeContext } from "../../ThemeContext";
import "./header.css";
import "./header-mobile.css";

const Header = ({ callBack, changePage }) => {
  const theme = useContext(ThemeContext);

  const [hamburgerClick, setHamburgerClick] = useState(true);

  return (
    <header className={"header " + theme}>
      <div className="header-flexbox">
        <h3>My React Project</h3>
        <div className="parent-nav-flexbox">
          <button
            id="hamburger"
            className={`hamburger hamburger-${theme}`}
            onClick={() => setHamburgerClick((click) => !click)}
          >
            <i class="gg-menu"></i>
          </button>

          <div
            className={`nav-flexbox ${
              hamburgerClick === true ? "nav-mobile-show" : "nav-mobile-noshow"
            } `}
          >
            <ul
              className={`header-navigation header-navigation-${theme} mobile-theme`}
            >
              <li onClick={() => changePage("home")}>Home</li>
              <li onClick={() => changePage("blogs")}>Blogs</li>
              <li onClick={() => changePage("about")}>About</li>
            </ul>
            <button
              className={`change-theme-button button-${theme}`}
              onClick={callBack}
            >
              {/* <i className="gg-sun"></i> */}
              {theme + " "}theme
            </button>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
