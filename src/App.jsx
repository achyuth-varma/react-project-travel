import React, { useState, useEffect, Suspense } from "react";
import "./App.css";
import FallBack from "./components/fallback";
import Footer from "./components/footer";
import Header from "./components/header";
import About from "./pages/About";
import Blogs from "./pages/Blogs";
import Home from "./pages/Home";
import { ThemeContext } from "./ThemeContext";
// const Home = lazyLoad("./pages/Home");

function App() {
  const [theme, setTheme] = useState("light");

  const [toggle, setToggle] = useState(false);

  const [currentPage, setCurrentPage] = useState("home");

  useEffect(() => {
    if (toggle) setTheme("dark");
    else setTheme("light");
  }, [toggle]);

  const changeTheme = () => {
    setToggle((toggle) => !toggle);
  };

  const changePage = (thePage) => {
    setCurrentPage(thePage);
  };

  return (
    <ThemeContext.Provider value={theme}>
      <div className={`the-root ${theme}-background "`}>
        <Header callBack={changeTheme} changePage={changePage} />
        <main className={`main-block main-${theme}`}>
          <Suspense fallback={<FallBack />}>
            {currentPage === "home" ? <Home callback={changePage} /> : ""}
            {currentPage === "blogs" ? <Blogs /> : ""}
            {currentPage === "about" ? <About /> : ""}
          </Suspense>
        </main>
        <Footer />
      </div>
    </ThemeContext.Provider>
  );
}

export default App;
